﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace ExchangeRate
{
    class RegressionModel
    {
        public double[,] TableIndісes;
        public double[,] TestTable;
        public double[] beta;
        public double[] y_marks;
        public double coef_determination;
        public double F_stat;
        public double F_table;
        public double[] res;

        public RegressionModel(string path, string StartCell, string FinCell, int numList, string startTestCell, string finTestCell)
        {
            this.TableIndісes = Form1.ReadingExcelTo2DArray(path, StartCell, FinCell, numList);
            this.TestTable = Form1.ReadingExcelTo2DArray(path, startTestCell, finTestCell, numList);
            this.beta = CalcOfBeta();
            this.y_marks = ArrayMarks();
            this.coef_determination = CoefDetermination();
            this.F_stat = F_observ();
            this.F_table = F_tab();
            this.res = Residals();
        }


        //Обчислення коефіцієнтів за допомогою МНК
        public double[] CalcOfBeta()
        {
            double[,] MarkTable = new double[this.TableIndісes.GetLength(0), 1];
            for (int i = 0; i < MarkTable.Length; i++)
            {
                MarkTable[i, 0] = this.TableIndісes[i, 0];
            }
            double[,] z = new double[this.TableIndісes.GetLength(0), this.TableIndісes.GetLength(1)];
            for (int i = 0; i < z.GetLength(0); i++)
            {
                z[i, 0] = 1;
                for (int j = 1; j < z.GetLength(1); j++)
                {
                    z[i, j] = this.TableIndісes[i, j];
                }
            }
            double[,] temp1 = MathOperations.TranspMatrix(z);
            double[,] temp2 = MathOperations.Multiplication(temp1, z);
            double[,] temp3 = MathOperations.Multiplication(temp1, MarkTable);
            double[] n_temp3 = new double[temp3.GetLength(0)];
            for (int i = 0; i < n_temp3.Length; i++)
            {
                n_temp3[i] = temp3[i, 0];
            }
            double[] beta = MathOperations.JordanGauss(temp2, n_temp3);
            return beta;
        }

        //Обчислення прогнозованого значення
        public double MarkValue(int numRow)
        {
            double mark = this.beta[0];
            for (int i = 1; i < beta.Length; i++)
            {
                mark += this.beta[i] * this.TestTable[numRow, i];
            }
            return Math.Round(mark, 4);
        }

        //Формування масиву прогнозованих значень
        public double[] ArrayMarks()
        {
            double[] y = new double[this.TestTable.GetLength(0)];
            for (int i = 0; i < y.Length; i++)
            {
                y[i] = MarkValue(i);
            }
            return y;
        }

        //Обчислення коефіцієнта детермінації
        public double CoefDetermination()
        {
            double avg = 0;
            for (int i = 0; i < this.TestTable.GetLength(0); i++)
            {
                avg += this.TestTable[i, 0];
            }
            avg /= this.TableIndісes.GetLength(0);

            double s_reg = 0;
            for (int i = 0; i < this.TestTable.GetLength(0); i++)
            {
                s_reg += Math.Pow(this.TestTable[i, 0] - this.y_marks[i], 2);
            }
            double s_total = 0;
            for (int i = 0; i < this.TestTable.GetLength(0); i++)
            {
                s_total += Math.Pow(this.TestTable[i, 0] - avg, 2);
            }
            return (1 - s_reg / s_total);
        }

        //Обчислення статистичного значення критерію Фішера
        public  double F_observ()
        {
            double f1 = this.coef_determination / (this.TestTable.GetLength(1) - 2);
            double f2 = (1 - this.coef_determination) / (this.TestTable.GetLength(0)
                - this.TestTable.GetLength(1) - 1);
            return f1 / f2;
        }

        //Обчислення критичного значення критерію Фішера
        public double F_tab()
        {
            Chart ch = new Chart();
            double result = ch.DataManipulator.Statistics.InverseFDistribution(0.05, this.TestTable.GetLength(1) - 2,
                this.TestTable.GetLength(0) - this.TestTable.GetLength(1) - 1);
            return result;
        }

        //Знаходження залишків
        public double[] Residals()
        {
            double[] res = new double[this.TestTable.GetLength(0)];
            for (int i = 0; i < this.y_marks.Length; i++)
            {
                res[i] = Math.Round(this.TestTable[i, 0] - this.y_marks[i], 4);
            }
            return res;
        }
    }
}
