﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExchangeRate
{
    class MathException : Exception
    {
        public MathException() { }
        public MathException(string message) : base(message) { }
        public MathException(string message, Exception ex) : base(message) { }
        protected MathException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext contex)
            : base(info, contex) { }
    }

    class MathOperations
    {
        //Транспонування матриці
        public static double[,] TranspMatrix(double[,] arr)
        {
            int CountRow = arr.GetLength(0);
            int CountColumn = arr.GetLength(1);
            double[,] mT = new double[CountColumn, CountRow];
            for (int i = 0; i < CountColumn; i++)
            {
                for (int j = 0; j < CountRow; j++)
                {
                    mT[i, j] = arr[j, i];
                }
            }
            return mT;
        }

        //Множення матриць
        public static double[,] Multiplication(double[,] Matrix1, double[,] Matrix2)
        {
            double[,] r = new double[Matrix1.GetLength(0), Matrix2.GetLength(1)];
            try
            {
                if (Matrix1.GetLength(1) != Matrix2.GetLength(0)) throw new MathException("Матриці не можна перемножити");
                for (int i = 0; i < Matrix1.GetLength(0); i++)
                {
                    for (int j = 0; j < Matrix2.GetLength(1); j++)
                    {
                        for (int k = 0; k < Matrix2.GetLength(0); k++)
                            r[i, j] += Matrix1[i, k] * Matrix2[k, j];
                    }
                }
            }
            catch (MathException e)
            {
                //Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
            }
            return r;
        }

        //Розв'язання СЛАР
        public static double[] JordanGauss(double[,] A, double[] b)
        {
            double[,] A1 = new double[A.GetLength(0), A.GetLength(1)];
            double[] b1 = new double[b.Length];
            Array.Copy(A, A1, A1.Length);
            Array.Copy(b, b1, b.Length);
            int n = b1.Length;
            for (int k = 0; k < n; k++)
            {
                double d = A1[k, k];
                A1[k, k] = 1;
                for (int j = k + 1; j < n; j++)
                {
                    A1[k, j] /= d;
                }
                b1[k] /= d;
                for (int i = 0; i < n; i++)
                {
                    if (i != k)
                    {
                        double S = A1[i, k];
                        A1[i, k] = 0;
                        for (int j = k + 1; j < n; j++)
                        {
                            A1[i, j] -= S * A1[k, j];
                        }
                        b1[i] -= S * b1[k];
                    }
                }
            }
            return b1;
        }
    }
}
