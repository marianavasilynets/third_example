﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace ExchangeRate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static string path1 = "SampleData.xlsx";
        string path = Path.GetFullPath(path1);
        RegressionModel reg_mod;

        public static double[,] ReadingExcelTo2DArray(string pathToFile, string StartCellD, string FinCellD, int numList)
        {
            Excel.Application ObjExcel = new Excel.Application();
            Excel.Workbook ObjWorkBook = ObjExcel.Workbooks.Open(pathToFile, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows,
                "", true, false, 0, true, false, false);
            Excel.Worksheet ObjWorkSheet;
            ObjWorkSheet = (Excel.Worksheet)ObjWorkBook.Sheets[numList];
            Excel.Range rng = ObjWorkSheet.get_Range(StartCellD, FinCellD);
            var dataArr = (object[,])rng.Value;
            dataArr.ToString();
            ObjWorkBook.Close(true);
            ObjExcel.Quit();
            double[,] data = new double[dataArr.GetLength(0), dataArr.GetLength(1)];
            for (int i = 1; i <= data.GetLength(0); i++)
            {
                for (int j = 1; j <= data.GetLength(1); j++)
                {
                    string s = dataArr[i, j].ToString();
                    s.Replace(',', '.');
                    data[i - 1, j - 1] = Convert.ToDouble(s);
                }
            }
            return data;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "F_стат: ";
            label2.Text = "F_крит: ";
            reg_mod = new RegressionModel(path, "E4", "N51", 2, "E54", "N77");

            dataGridView1.RowCount = reg_mod.y_marks.Length;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1[0, i].Value = reg_mod.TestTable[i, 0];
                dataGridView1[1, i].Value = reg_mod.y_marks[i];
                dataGridView1[2, i].Value = reg_mod.res[i];
            }
            double f_stat = Math.Round(reg_mod.F_stat, 4);
            double f_tab = Math.Round(reg_mod.F_table, 4);
            label1.Text += f_stat.ToString();
            label2.Text += f_tab.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.RowCount = 18;
        }
    }
}
